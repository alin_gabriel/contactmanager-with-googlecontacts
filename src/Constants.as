package
{
	public class Constants
	{
		public static const RIGHT_MARGIN:Number = 10;
		public static const RATIO_FOR_INPUT:Number = 5 / 12;
		public static const RATIO_FOR_INPUT_WIDTH:Number = 7 / 12;
		public static const RATIO_FOR_USER:Number = 7 / 16;
		public static const RATIO_FOR_PASSWORD:Number = 9 / 16;
		
		public function Constants()
		{
		}
	}
}