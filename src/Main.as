package 
{
	import com.bestsoftwareever.contactmanager.commands.AddContactCommand;
	import com.bestsoftwareever.contactmanager.commands.AuthenticationCommand;
	import com.bestsoftwareever.contactmanager.commands.DeleteContactCommand;
	import com.bestsoftwareever.contactmanager.commands.EditContactCommand;
	import com.bestsoftwareever.contactmanager.commands.GetContactCommand;
	import com.bestsoftwareever.contactmanager.commands.GetContactsCommand;
	import com.bestsoftwareever.contactmanager.commands.LoginCommand;
	import com.bestsoftwareever.contactmanager.commands.SetViewCommand;
	import com.bestsoftwareever.contactmanager.commands.StoreDataCommand;
	import com.bestsoftwareever.contactmanager.controller.Controller;
	import com.bestsoftwareever.contactmanager.events.AddContactEvent;
	import com.bestsoftwareever.contactmanager.events.AuthenticationEvent;
	import com.bestsoftwareever.contactmanager.events.DeleteContactEvent;
	import com.bestsoftwareever.contactmanager.events.EditContactEvent;
	import com.bestsoftwareever.contactmanager.events.GetContactEvent;
	import com.bestsoftwareever.contactmanager.events.GetContactsEvent;
	import com.bestsoftwareever.contactmanager.events.LoginEvent;
	import com.bestsoftwareever.contactmanager.events.SetViewEvent;
	import com.bestsoftwareever.contactmanager.events.StoreDataEvent;
	import com.bestsoftwareever.contactmanager.model.Contact;
	import com.bestsoftwareever.contactmanager.model.ContactsModel;
	import com.bestsoftwareever.contactmanager.model.ModelLocator;
	import com.bestsoftwareever.contactmanager.model.ViewsModel;
	import com.bestsoftwareever.contactmanager.views.AddContactView;
	import com.bestsoftwareever.contactmanager.views.AuthenticationView;
	import com.bestsoftwareever.contactmanager.views.ContactDetails;
	import com.bestsoftwareever.contactmanager.views.ContactsList;
	import com.bestsoftwareever.contactmanager.views.EditContactView;
	import com.bestsoftwareever.contactmanager.views.LoginView;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	
	
	[SWF(width="500", height="800", backgroundColor="#ffffff", frameRate="30")]
	
	public class Main extends Sprite
	{
		private var login:LoginView;
		private var control:Controller;
		
		private var modelLocator:ModelLocator;
		private var vm:ViewsModel;
		
		public function Main()
		{
			stage.align = StageAlign.TOP_LEFT;
			
			modelLocator = ModelLocator.getInstance();
			vm = new ViewsModel();
			vm.setContainer(stage);
			
			modelLocator.addModel(ViewsModel.NAME,vm);
			
			vm.addView(LoginView.NAME,LoginView);
			vm.addView(AddContactView.NAME,AddContactView);
			vm.addView(EditContactView.NAME,EditContactView);
			vm.addView(ContactsList.NAME,ContactsList);
			vm.addView(AuthenticationView.NAME,AuthenticationView);
			vm.addView(ContactDetails.NAME,ContactDetails);
			
			control = Controller.getInstance();
			control.addCommand(LoginEvent.NAME, LoginCommand);
			control.addCommand(SetViewEvent.NAME, SetViewCommand);
			control.addCommand(AddContactEvent.NAME, AddContactCommand);
			control.addCommand(EditContactEvent.NAME, EditContactCommand);
			control.addCommand(StoreDataEvent.NAME, StoreDataCommand);
			control.addCommand(DeleteContactEvent.NAME, DeleteContactCommand);
			control.addCommand(AuthenticationEvent.NAME, AuthenticationCommand);
			control.addCommand(GetContactsEvent.NAME, GetContactsCommand);
			control.addCommand(GetContactEvent.NAME, GetContactCommand);
			
			control.dispatchEvent(new SetViewEvent(LoginView.NAME));
		}
	}
}