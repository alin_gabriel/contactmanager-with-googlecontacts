package com.bestsoftwareever.contactmanager.views
{
	import com.bestsoftwareever.contactmanager.events.EditContactEvent;
	import com.bestsoftwareever.contactmanager.events.SetViewEvent;
	import com.bestsoftwareever.contactmanager.model.Contact;
	import com.bestsoftwareever.contactmanager.model.ContactsModel;
	import com.bestsoftwareever.contactmanager.model.ModelBase;
	import com.bestsoftwareever.contactmanager.model.ModelLocator;
	import com.bestsoftwareever.contactmanager.model.ViewsModel;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.net.FileFilter;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;

	public class EditContactView extends View
	{
		public static const NAME:String = "editContactView";
		private var editContactLabel:TextField;
		private var firstNameLabel:TextField;
		public var firstName:TextField;
		private var middleNameLabel:TextField;
		public var middleName:TextField;
		private var lastNameLabel:TextField;
		public var lastName:TextField;
		private var ageLabel:TextField;
		public var age:TextField;
		private var phoneNumberLabel:TextField;
		public var phoneNumber:TextField;
		private var professionLabel:TextField;
		public var profession:TextField;
		private var backButton:TextField;
		private var saveButton:TextField;
		private var contactsModel:ContactsModel;
		private var contactModel:Contact;

		public var editedContact:Contact;
		private var pictureLabel:TextField;
		public var picture:Loader;
		private var changePictureButton:TextField;
		public var file:File;
		public var filePath:File = null;
		public var pictureChanged:Boolean = false;
		
		public function EditContactView()
		{
			skin = new Sprite();
			super(skin);
			
			contactsModel = ModelLocator.getInstance().getModel(ContactsModel.NAME) as ContactsModel;
			if ( contactsModel != null )
			{
				editedContact = contactsModel.selectedContact.clone();
				this.container = (ModelLocator.getInstance().getModel(ViewsModel.NAME) as ViewsModel).container;
				
				editContactLabel = new TextField();
				editContactLabel.defaultTextFormat = new TextFormat("Arial",30,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
				editContactLabel.text = "Edit contact";
				editContactLabel.width = 300;
				editContactLabel.height = 35;
				editContactLabel.selectable = false;
				
				firstNameLabel = new TextField();
				firstNameLabel.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
				firstNameLabel.text = "First name";
				firstNameLabel.width = container.stage.stageWidth / 3;
				firstNameLabel.height = 20;
				firstNameLabel.x = 0;
				firstNameLabel.y = 80;
				firstNameLabel.selectable = false;
				
				firstName = new TextField();
				firstName.defaultTextFormat = new TextFormat("Arial",14,0x0,null,null,null,null,null,TextFormatAlign.CENTER);
				firstName.text = "";
				firstName.width = Constants.RATIO_FOR_INPUT_WIDTH * container.stage.stageWidth - Constants.RIGHT_MARGIN;
				firstName.height = 20;
				firstName.x = Constants.RATIO_FOR_INPUT * container.stage.stageWidth;
				firstName.y = 80;
				firstName.border = true;
				firstName.type = "input";
				
				middleNameLabel = new TextField();
				middleNameLabel.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
				middleNameLabel.text = "Middle name";
				middleNameLabel.width = container.stage.stageWidth / 3;
				middleNameLabel.height = 20;
				middleNameLabel.x = 0;
				middleNameLabel.y = 130;
				middleNameLabel.selectable = false;
				
				middleName = new TextField();
				middleName.defaultTextFormat = new TextFormat("Arial",14,0x0,null,null,null,null,null,TextFormatAlign.CENTER);
				middleName.text = "";
				middleName.width = Constants.RATIO_FOR_INPUT_WIDTH * container.stage.stageWidth - Constants.RIGHT_MARGIN;
				middleName.height = 20;
				middleName.x = Constants.RATIO_FOR_INPUT * container.stage.stageWidth;
				middleName.y = 130;
				middleName.border = true;
				middleName.type = "input";
				
				lastNameLabel = new TextField();
				lastNameLabel.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
				lastNameLabel.text = "Last name";
				lastNameLabel.width = container.stage.stageWidth / 3;
				lastNameLabel.height = 20;
				lastNameLabel.x = 0;
				lastNameLabel.y = 180;
				lastNameLabel.selectable = false;
				
				lastName = new TextField();
				lastName.defaultTextFormat = new TextFormat("Arial",14,0x0,null,null,null,null,null,TextFormatAlign.CENTER);
				lastName.text = "";
				lastName.width = Constants.RATIO_FOR_INPUT_WIDTH * container.stage.stageWidth - Constants.RIGHT_MARGIN;
				lastName.height = 20;
				lastName.x = Constants.RATIO_FOR_INPUT * container.stage.stageWidth;
				lastName.y = 180;
				lastName.border = true;
				lastName.type = "input";
				
				ageLabel = new TextField();
				ageLabel.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
				ageLabel.text = "Age";
				ageLabel.width = container.stage.stageWidth / 3;
				ageLabel.height = 25;
				ageLabel.x = 0;
				ageLabel.y = 230;
				ageLabel.selectable = false;
				
				age = new TextField();
				age.defaultTextFormat = new TextFormat("Arial",14,0x0,null,null,null,null,null,TextFormatAlign.CENTER);
				age.text = "";
				age.width = Constants.RATIO_FOR_INPUT_WIDTH * container.stage.stageWidth - Constants.RIGHT_MARGIN;
				age.height = 20;
				age.x = Constants.RATIO_FOR_INPUT * container.stage.stageWidth;
				age.y = 230;
				age.border = true;
				age.type = "input";
				
				phoneNumberLabel = new TextField();
				phoneNumberLabel.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
				phoneNumberLabel.text = "Phone";
				phoneNumberLabel.width = container.stage.stageWidth / 3;
				phoneNumberLabel.height = 25;
				phoneNumberLabel.x = 0;
				phoneNumberLabel.y = 280;
				phoneNumberLabel.selectable = false;
				
				phoneNumber = new TextField();
				phoneNumber.defaultTextFormat = new TextFormat("Arial",14,0x0,null,null,null,null,null,TextFormatAlign.CENTER);
				phoneNumber.text = "";
				phoneNumber.width = Constants.RATIO_FOR_INPUT_WIDTH * container.stage.stageWidth - Constants.RIGHT_MARGIN;
				phoneNumber.height = 20;
				phoneNumber.x = Constants.RATIO_FOR_INPUT * container.stage.stageWidth;
				phoneNumber.y = 280;
				phoneNumber.border = true;
				phoneNumber.type = "input";
				
				professionLabel = new TextField();
				professionLabel.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
				professionLabel.text = "Profession";
				professionLabel.width = container.stage.stageWidth / 3;
				professionLabel.height = 25;
				professionLabel.x = 0;
				professionLabel.y = 330;
				professionLabel.selectable = false;
				
				profession = new TextField();
				profession.defaultTextFormat = new TextFormat("Arial",14,0x0,null,null,null,null,null,TextFormatAlign.CENTER);
				profession.text = "";
				profession.width = Constants.RATIO_FOR_INPUT_WIDTH * container.stage.stageWidth - Constants.RIGHT_MARGIN;
				profession.height = 20;
				profession.x = Constants.RATIO_FOR_INPUT * container.stage.stageWidth;
				profession.y = 330;
				profession.border = true;
				profession.type = "input";
				
				pictureLabel = new TextField();
				pictureLabel.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
				pictureLabel.text = "Picture: ";
				pictureLabel.width = container.stage.stageWidth / 3;
				pictureLabel.height = 25;
				pictureLabel.x = 10;
				pictureLabel.y = 380;
				pictureLabel.selectable = false;
				
				if ( editedContact.picture != null )
				{
					picture = new Loader();
					picture.load(new URLRequest(editedContact.picture));
					picture.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoadComplete);
				}
				
				backButton = new TextField();
				backButton.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
				backButton.text = "Back";
				backButton.width = 80;
				backButton.height = 20;
				backButton.border = true;
				backButton.x = 20;
				backButton.y = container.stage.stageHeight - 50;
				backButton.selectable = false;
				
				saveButton = new TextField();
				saveButton.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
				saveButton.text = "Save";
				saveButton.width = 80;
				saveButton.height = 20;
				saveButton.border = true;
				saveButton.x = container.stage.stageWidth - 80 - 20;
				saveButton.y = container.stage.stageHeight - 50;
				saveButton.selectable = false;
				
				changePictureButton = new TextField();
				changePictureButton.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
				changePictureButton.text = "Change Picture";
				changePictureButton.width = 80;
				changePictureButton.height = 45;
				changePictureButton.border = true;
				changePictureButton.x = container.stage.stageWidth / 6 - changePictureButton.width / 2;
				changePictureButton.y = 430;
				changePictureButton.selectable = false;
				changePictureButton.wordWrap = true;
				
				skin.addChild(editContactLabel);
				skin.addChild(firstNameLabel);
				skin.addChild(firstName);
				skin.addChild(middleNameLabel);
				skin.addChild(middleName);
				skin.addChild(lastNameLabel);
				skin.addChild(lastName);
				skin.addChild(ageLabel);
				skin.addChild(age);
				skin.addChild(phoneNumberLabel);
				skin.addChild(phoneNumber);
				skin.addChild(professionLabel);
				skin.addChild(profession);
				skin.addChild(pictureLabel);
				skin.addChild(backButton);
				skin.addChild(saveButton);
				skin.addChild(changePictureButton);
				
				backButton.addEventListener(MouseEvent.CLICK, onBackButtonClick);
				saveButton.addEventListener(MouseEvent.CLICK, onSaveButtonClick);
				changePictureButton.addEventListener(MouseEvent.CLICK, onChangePictureButtonClick);
			}
		}
		
		protected function onChangePictureButtonClick(event:MouseEvent):void
		{
			file = new File();
			var imageFileTypes:FileFilter = new FileFilter("Images (*.jpg, *.png)", "*.jpg;*.png");
			
			file.browse([imageFileTypes]);
			file.addEventListener(Event.SELECT, selectFile);
		}
		
		protected function selectFile(event:Event):void
		{
			file.load();
			file.addEventListener(Event.COMPLETE, onLoadPictureComplete);
		}
		
		protected function onLoadPictureComplete(event:Event):void
		{
			
			pictureChanged = true;
			
			//Preview the picture
			if ( picture )
				skin.removeChild(picture);
			picture = new Loader();
			picture.load(new URLRequest(file.nativePath));
			picture.contentLoaderInfo.addEventListener(Event.COMPLETE, onPreviewComplete);
		}
		
		protected function onPreviewComplete(event:Event):void
		{
			picture.x = Constants.RATIO_FOR_INPUT * container.stage.stageWidth;
			picture.y = 380;
			picture.scaleX = (container.stage.stageWidth / 2 - 30) / picture.width;
			picture.scaleY = (container.stage.stageWidth / 2 - 30) / picture.height;
			skin.addChild(picture);
		}
		
		public function onLoadComplete(event:Event):void
		{
			picture.x = Constants.RATIO_FOR_INPUT * container.stage.stageWidth;
			picture.y = 380;
			picture.scaleX = (container.stage.stageWidth / 2 - 30) / picture.width;
			picture.scaleY = (container.stage.stageWidth / 2 - 30) / picture.height;
			skin.addChild(picture);
		}
		
		protected function onSaveButtonClick(event:MouseEvent):void
		{	
			dispatcher.dispatchEvent(new EditContactEvent(EditContactEvent.NAME,editedContact));
			dispatcher.dispatchEvent(new SetViewEvent(ContactsList.NAME));
		}
		
		protected function onBackButtonClick(event:MouseEvent):void
		{
			dispatcher.dispatchEvent(new SetViewEvent(ContactsList.NAME));
		}
	}
}