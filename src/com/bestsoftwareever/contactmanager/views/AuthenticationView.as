package com.bestsoftwareever.contactmanager.views
{
	import com.bestsoftwareever.contactmanager.controller.Controller;
	import com.bestsoftwareever.contactmanager.events.AuthenticationEvent;
	import com.bestsoftwareever.contactmanager.events.SetViewEvent;
	import com.bestsoftwareever.contactmanager.model.ModelLocator;
	import com.bestsoftwareever.contactmanager.model.ViewsModel;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.geom.Rectangle;
	import flash.media.StageWebView;
	
	public class AuthenticationView extends View
	{
		public static var NAME:String = "authentication";
		public var stageWebView:StageWebView;
		
		public function AuthenticationView()
		{
			skin = new Sprite();
			super(skin);
			
			stageWebView = new StageWebView();
			stageWebView.stage = (ModelLocator.getInstance().getModel(ViewsModel.NAME) as ViewsModel).container as Stage;
			var rectangle:Rectangle = new Rectangle(0,0,stageWebView.stage.stageWidth,stageWebView.stage.stageHeight);
			if (rectangle.width > 0 && rectangle.height > 0)
			{
				stageWebView.viewPort = rectangle;
			}			
		}
	}
}