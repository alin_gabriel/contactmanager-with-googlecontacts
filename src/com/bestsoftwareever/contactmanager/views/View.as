package com.bestsoftwareever.contactmanager.views
{
	import com.bestsoftwareever.contactmanager.controller.Controller;
	import com.bestsoftwareever.contactmanager.model.ViewsModel;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IEventDispatcher;

	public class View extends ViewsModel
	{
		protected var dispatcher:IEventDispatcher;
		public var skin:DisplayObjectContainer;
		
		public function View(skin:DisplayObjectContainer)
		{
			this.skin = skin;
			skin.addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		protected function initialize(event:Event):void {
			dispatcher = Controller.getInstance();
		}
		
		public function remove():void {
			if (skin.parent) {
				skin.parent.removeChild(skin);
			}
		}
	}
}