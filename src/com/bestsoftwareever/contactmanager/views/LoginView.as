package com.bestsoftwareever.contactmanager.views
{
	import com.bestsoftwareever.contactmanager.events.LoginEvent;
	import com.bestsoftwareever.contactmanager.model.ModelLocator;
	import com.bestsoftwareever.contactmanager.model.ViewsModel;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import org.osmf.elements.ImageLoader;

	public class LoginView extends View
	{
		public var userText:TextField;
		public var passwordText:TextField;
		public var labelUser:TextField;
		public var labelPassword:TextField;
		public var loginImage:Loader;
		
		private var loginEvent:LoginEvent;
		public static var NAME:String = "loginView";
		
		public function LoginView()
		{
			skin = new Sprite();
			super(skin);
			this.container = (ModelLocator.getInstance().getModel(ViewsModel.NAME) as ViewsModel).container;
			
			userText = new TextField();
			userText.defaultTextFormat = new TextFormat("Arial",12,0x0);
			userText.text = "";
			userText.border = true;
			userText.width = container.stage.stageWidth / 3;
			userText.height = 20;
			userText.x = container.stage.stageWidth / 3;
			userText.y = Constants.RATIO_FOR_USER * container.stage.stageHeight;
			userText.type = "input";
			
			passwordText = new TextField();
			passwordText.defaultTextFormat = new TextFormat("Arial",12,0x0);
			passwordText.text = "";
			passwordText.border = true;
			passwordText.width = container.stage.stageWidth / 3;
			passwordText.height = 20;
			passwordText.x = container.stage.stageWidth / 3;
			passwordText.y = Constants.RATIO_FOR_PASSWORD * container.stage.stageHeight;
			passwordText.displayAsPassword = true;
			passwordText.type = "input";
			
			labelUser = new TextField();
			labelUser.defaultTextFormat = new TextFormat("Arial",12,0x0,null,null,null,null,null,TextFormatAlign.CENTER);
			labelUser.text = "Username";
			labelUser.x = container.stage.stageWidth / 3;
			labelUser.y = Constants.RATIO_FOR_USER * container.stage.stageHeight - userText.height;
			labelUser.width = container.stage.stageWidth / 3;
			labelUser.height = 20;
			
			labelPassword = new TextField();
			labelPassword.defaultTextFormat = new TextFormat("Arial",12,0x0,null,null,null,null,null,TextFormatAlign.CENTER);
			labelPassword.text = "Password";
			labelPassword.x = container.stage.stageWidth / 3;;
			labelPassword.y = Constants.RATIO_FOR_PASSWORD * container.stage.stageHeight - passwordText.height;
			labelPassword.width = container.stage.stageWidth / 3;
			labelPassword.height = 20;
			
			loginImage = new Loader();
			loginImage.load(new URLRequest("/Images/login.jpg"));
			loginImage.contentLoaderInfo.addEventListener(Event.COMPLETE,onLoadComplete);
			
			skin.addChild(userText);
			skin.addChild(passwordText);
			skin.addChild(labelUser);
			skin.addChild(labelPassword);
			
			loginImage.addEventListener(MouseEvent.CLICK, onClickLoginButton);
		}
		
		protected function onClickLoginButton(event:MouseEvent):void
		{
			loginEvent = new LoginEvent(userText.text,passwordText.text);
			dispatcher.dispatchEvent(loginEvent);
		}
		
		protected function onLoadComplete(event:Event):void
		{
			loginImage.scaleX = loginImage.scaleY = 0.3;
			loginImage.x = container.stage.stageWidth / 2 - loginImage.width / 2;
			loginImage.y = 4 * container.stage.stageHeight / 5; 
			skin.addChild(loginImage);
		}
	}
}