package com.bestsoftwareever.contactmanager.views
{
	import com.bestsoftwareever.contactmanager.events.AddContactEvent;
	import com.bestsoftwareever.contactmanager.events.SetViewEvent;
	import com.bestsoftwareever.contactmanager.model.Contact;
	import com.bestsoftwareever.contactmanager.model.ContactsModel;
	import com.bestsoftwareever.contactmanager.model.ModelLocator;
	import com.bestsoftwareever.contactmanager.model.ViewsModel;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;

	public class AddContactView extends View
	{
		public static const NAME:String = "addContactView";
		public var contactModel:Contact = new Contact();
		
		private var addContactLabel:TextField;
		private var firstNameLabel:TextField;
		public var firstName:TextField;
		public var middleName:TextField;
		private var middleNameLabel:TextField;
		public var lastName:TextField;
		private var lastNameLabel:TextField;
		public var age:TextField;
		private var ageLabel:TextField;
		public var phoneNumber:TextField;
		private var phoneNumberLabel:TextField;
		private var professionLabel:TextField;
		public var profession:TextField;
		private var backButton:TextField;
		private var saveButton:TextField;
		private var loadPicture:TextField;
		private var file:File;
		public var filePath:File = null;
		
		public function AddContactView()
		{
			skin = new Sprite();
			super(skin);
			this.container = (ModelLocator.getInstance().getModel(ViewsModel.NAME) as ViewsModel).container;
			
			addContactLabel = new TextField();
			addContactLabel.defaultTextFormat = new TextFormat("Arial",30,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
			addContactLabel.text = "Add contact";
			addContactLabel.width = container.stage.stageWidth;
			addContactLabel.height = 35;
			addContactLabel.selectable = false;
			
			firstNameLabel = new TextField();
			firstNameLabel.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
			firstNameLabel.text = "First name";
			firstNameLabel.width = container.stage.stageWidth / 3;
			firstNameLabel.height = 20;
			firstNameLabel.x = 0;
			firstNameLabel.y = 80;
			firstNameLabel.selectable = false;
			
			firstName = new TextField();
			firstName.defaultTextFormat = new TextFormat("Arial",14,0x0,null,null,null,null,null,TextFormatAlign.CENTER);
			firstName.text = "";
			firstName.width = Constants.RATIO_FOR_INPUT_WIDTH * container.stage.stageWidth - Constants.RIGHT_MARGIN;
			firstName.height = 20;
			firstName.x = Constants.RATIO_FOR_INPUT * container.stage.stageWidth;
			firstName.y = 80;
			firstName.border = true;
			firstName.type = "input";
			
			middleNameLabel = new TextField();
			middleNameLabel.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
			middleNameLabel.text = "Middle name";
			middleNameLabel.width = container.stage.stageWidth / 3;
			middleNameLabel.height = 20;
			middleNameLabel.x = 0;
			middleNameLabel.y = 130;
			middleNameLabel.selectable = false;
			
			middleName = new TextField();
			middleName.defaultTextFormat = new TextFormat("Arial",14,0x0,null,null,null,null,null,TextFormatAlign.CENTER);
			middleName.text = "";
			middleName.width = Constants.RATIO_FOR_INPUT_WIDTH * container.stage.stageWidth - Constants.RIGHT_MARGIN;
			middleName.height = 20;
			middleName.x = Constants.RATIO_FOR_INPUT * container.stage.stageWidth;
			middleName.y = 130;
			middleName.border = true;
			middleName.type = "input";
			
			lastNameLabel = new TextField();
			lastNameLabel.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
			lastNameLabel.text = "Last name";
			lastNameLabel.width = container.stage.stageWidth / 3;
			lastNameLabel.height = 20;
			lastNameLabel.x = 0;
			lastNameLabel.y = 180;
			lastNameLabel.selectable = false;
			
			lastName = new TextField();
			lastName.defaultTextFormat = new TextFormat("Arial",14,0x0,null,null,null,null,null,TextFormatAlign.CENTER);
			lastName.text = "";
			lastName.width = Constants.RATIO_FOR_INPUT_WIDTH * container.stage.stageWidth - Constants.RIGHT_MARGIN;
			lastName.height = 20;
			lastName.x = Constants.RATIO_FOR_INPUT * container.stage.stageWidth;
			lastName.y = 180;
			lastName.border = true;
			lastName.type = "input";
			
			ageLabel = new TextField();
			ageLabel.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
			ageLabel.text = "Age";
			ageLabel.width = container.stage.stageWidth / 3;
			ageLabel.height = 25;
			ageLabel.x = 0;
			ageLabel.y = 230;
			ageLabel.selectable = false;
			
			age = new TextField();
			age.defaultTextFormat = new TextFormat("Arial",14,0x0,null,null,null,null,null,TextFormatAlign.CENTER);
			age.text = "";
			age.width = Constants.RATIO_FOR_INPUT_WIDTH * container.stage.stageWidth - Constants.RIGHT_MARGIN;
			age.height = 20;
			age.x = Constants.RATIO_FOR_INPUT * container.stage.stageWidth;
			age.y = 230;
			age.border = true;
			age.type = "input";
			
			phoneNumberLabel = new TextField();
			phoneNumberLabel.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
			phoneNumberLabel.text = "Phone";
			phoneNumberLabel.width = container.stage.stageWidth / 3;
			phoneNumberLabel.height = 25;
			phoneNumberLabel.x = 0;
			phoneNumberLabel.y = 280;
			phoneNumberLabel.selectable = false;
			
			phoneNumber = new TextField();
			phoneNumber.defaultTextFormat = new TextFormat("Arial",14,0x0,null,null,null,null,null,TextFormatAlign.CENTER);
			phoneNumber.text = "";
			phoneNumber.width = Constants.RATIO_FOR_INPUT_WIDTH * container.stage.stageWidth - Constants.RIGHT_MARGIN;
			phoneNumber.height = 20;
			phoneNumber.x = Constants.RATIO_FOR_INPUT * container.stage.stageWidth;
			phoneNumber.y = 280;
			phoneNumber.border = true;
			phoneNumber.type = "input";
			
			professionLabel = new TextField();
			professionLabel.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
			professionLabel.text = "Profession";
			professionLabel.width = container.stage.stageWidth / 3;
			professionLabel.height = 25;
			professionLabel.x = 0;
			professionLabel.y = 330;
			professionLabel.selectable = false;
			
			profession = new TextField();
			profession.defaultTextFormat = new TextFormat("Arial",14,0x0,null,null,null,null,null,TextFormatAlign.CENTER);
			profession.text = "";
			trace(profession.width);
			profession.width = Constants.RATIO_FOR_INPUT_WIDTH * container.stage.stageWidth - Constants.RIGHT_MARGIN;
			profession.height = 20;
			profession.x = Constants.RATIO_FOR_INPUT * container.stage.stageWidth;
			profession.y = 330;
			profession.border = true;
			profession.type = "input";
			
			loadPicture = new TextField();
			loadPicture.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
			loadPicture.text = "Load picture";
			loadPicture.width = container.stage.stageWidth / 2;
			loadPicture.height = 25;
			loadPicture.x = container.stage.stageWidth / 4;
			loadPicture.y = 380;
			loadPicture.selectable = false;
			loadPicture.border = true;
			
			backButton = new TextField();
			backButton.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
			backButton.text = "Back";
			backButton.width = 80;
			backButton.height = 20;
			backButton.border = true;
			backButton.x = 20;
			backButton.y = 420;
			backButton.selectable = false;
			
			saveButton = new TextField();
			saveButton.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
			saveButton.text = "Save";
			saveButton.width = 80;
			saveButton.height = 20;
			saveButton.border = true;
			saveButton.x = container.stage.stageWidth - 80 - 20;
			saveButton.y = 420;
			saveButton.selectable = false;
			
			skin.addChild(addContactLabel);
			skin.addChild(firstNameLabel);
			skin.addChild(firstName);
			skin.addChild(middleNameLabel);
			skin.addChild(middleName);
			skin.addChild(lastNameLabel);
			skin.addChild(lastName);
			skin.addChild(ageLabel);
			skin.addChild(age);
			skin.addChild(phoneNumberLabel);
			skin.addChild(phoneNumber);
			skin.addChild(professionLabel);
			skin.addChild(profession);
			skin.addChild(loadPicture);
			skin.addChild(backButton);
			skin.addChild(saveButton);
			
			backButton.addEventListener(MouseEvent.CLICK, onBackButtonClick);
			saveButton.addEventListener(MouseEvent.CLICK, onSaveButtonClick);
			loadPicture.addEventListener(MouseEvent.CLICK, onLoadPictureClick);
		}
		
		override protected function initialize(event:Event):void {
			contactModel = new Contact();
			super.initialize(event);
		}
		
		protected function onLoadPictureClick(event:MouseEvent):void
		{
			file = new File();
			var imageFileTypes:FileFilter = new FileFilter("Images (*.jpg, *.png)", "*.jpg;*.png");
			
			file.browse([imageFileTypes]);
			file.addEventListener(Event.SELECT, selectFile);
		}
		
		protected function selectFile(event:Event):void
		{
			file.load();
			file.addEventListener(Event.COMPLETE, onLoadPictureComplete);
		}
		
		protected function onLoadPictureComplete(event:Event):void
		{
			filePath = new File().resolvePath("D:\\Work\\Actionscript\\Contact Manager\\src\\Contacts Pictures\\" + contactModel.id + ".jpg");
			file.copyTo(filePath,true); 
		}
		
		protected function onSaveButtonClick(event:MouseEvent):void
		{	
			dispatcher.dispatchEvent(new AddContactEvent(AddContactEvent.NAME)); 
			dispatcher.dispatchEvent(new SetViewEvent(ContactsList.NAME));
		}
		
		protected function onBackButtonClick(event:MouseEvent):void
		{
			var contactsList:SetViewEvent = new SetViewEvent(ContactsList.NAME);
			dispatcher.dispatchEvent(contactsList);
		}
	}
}