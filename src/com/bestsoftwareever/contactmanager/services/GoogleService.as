package com.bestsoftwareever.contactmanager.services
{
	import com.bestsoftwareever.contactmanager.events.SetViewEvent;
	import com.bestsoftwareever.contactmanager.model.ContactsModel;
	import com.bestsoftwareever.contactmanager.model.ModelLocator;
	import com.bestsoftwareever.contactmanager.model.ViewsModel;
	import com.bestsoftwareever.contactmanager.views.ContactsList;
	
	import flash.net.Responder;
	import flash.net.URLRequestMethod;
	import flash.ui.GameInput;
	
	import grant.IGrantType;
	
	import mx.messaging.messages.HTTPRequestMessage;
	import mx.rpc.AsyncToken;
	import mx.rpc.Responder;
	import mx.rpc.http.HTTPService;
	
	public class GoogleService extends HTTPService
	{
		private var userEmail:String;
		public var accessToken:String;
		public var oauth2:OAuth2 = new OAuth2("https://accounts.google.com/o/oauth2/auth", "https://accounts.google.com/o/oauth2/token");
		public var result:Object;
		
		private static var INSTANCE:GoogleService;

		public function GoogleService(se:SingletonEnforcer) {
			if (!se is SingletonEnforcer) {
				throw new Error("Controller cannot be instantiated");
			}
		}
		
		public static function getInstance():GoogleService {
			if (!INSTANCE) {
				INSTANCE = new GoogleService(new SingletonEnforcer());
			}
			return INSTANCE;
		}
		
		public function login(username:String,password:String):void 
		{
			userEmail = username;
			ModelLocator.getInstance().addModel(ContactsModel.NAME, new ContactsModel);
		}
		
		public function getContacts(startIndex:int = 1,itemsPerPage:int = 25):AsyncToken
		{
			method = URLRequestMethod.GET;
			url = "https://www.google.com/m8/feeds/contacts/default/full";
			var params:Object = {}
			params.access_token = accessToken;
			params.alt = "json";
			params["start-index"] = startIndex;
			params["max-results"] = itemsPerPage;
			return send(params);
		}
		
		public function getPhoto(id:String):AsyncToken
		{
			method = URLRequestMethod.GET;
			url = "https://www.google.com/m8/feeds/photos/media/default/" + id;
			var params:Object = {}
			params.access_token = accessToken;
			return send(params);
		}
		
		public function getContact(id:String):AsyncToken 
		{
			method = URLRequestMethod.GET;
			url = "https://www.google.com/m8/feeds/contacts/default/full/" + id;
			var params:Object = {};
			params.access_token = accessToken;
			params.alt = "json";
			return send(params);
		}
		
		public function createContact(username:String, contact:Object):AsyncToken {
			method = URLRequestMethod.POST;
			url = "https://www.google.com/m8/feeds/contacts/"+username+"/full/";
			return send();
		}
	}
}

class SingletonEnforcer
{
}