package com.bestsoftwareever.contactmanager.events
{
	import flash.events.Event;
	
	public class GetContactsEvent extends Event
	{
		public static const NAME:String = "getContactsEvent";
		
		public function GetContactsEvent()
		{
			super(NAME);
		}
	}
}