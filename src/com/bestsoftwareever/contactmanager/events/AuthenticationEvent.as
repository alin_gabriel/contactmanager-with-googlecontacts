package com.bestsoftwareever.contactmanager.events
{
	import flash.events.Event;

	public class AuthenticationEvent extends Event
	{
		public static const NAME:String = "authenticationEvent";
		
		public function AuthenticationEvent(name:String)
		{
			super(name);
		}
	}
}