package com.bestsoftwareever.contactmanager.events
{
	import com.bestsoftwareever.contactmanager.model.Contact;
	
	import flash.events.Event;
	
	public class DeleteContactEvent extends Event
	{
		public static const NAME:String = "deleteContact";
		public var contact:Contact;
		
		public function DeleteContactEvent(name:String)
		{
			super(name);
			this.contact = contact;
		}
	}
}