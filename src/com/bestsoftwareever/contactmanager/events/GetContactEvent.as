package com.bestsoftwareever.contactmanager.events
{
	import flash.events.Event;

	public class GetContactEvent extends Event
	{
		public static const NAME:String = "getContact";
		public var id:String;
		
		public function GetContactEvent(id:String)
		{
			super(NAME);
			this.id = id;
		}
	}
}