package com.bestsoftwareever.contactmanager.events
{
	import flash.events.Event;
	import com.bestsoftwareever.contactmanager.views.LoginView;

	public class LoginEvent extends Event
	{
		public static const NAME:String = "LoginEvent";		
		public var username:String;
		public var password:String;
		
		public function LoginEvent(username:String,password:String)
		{
			this.username = username;
			this.password = password;
			super(NAME);
		}
		
	}
}