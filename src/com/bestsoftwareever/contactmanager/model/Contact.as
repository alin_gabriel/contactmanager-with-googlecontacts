package com.bestsoftwareever.contactmanager.model
{
	import flash.display.Loader;
	import flash.filesystem.File;

	public class Contact 
	{
		public var id:String = "";
		public var firstName:String = "";
		public var middleName:String = "";
		public var lastName:String = "";
		public var age:uint = 0;
		public var phoneNumber:String = "";
		public var profession:String = "";
		public var picture:String;
		public var email:String = "";
		
		public function Contact()
		{
			
		}
		
		public function toString():String {
			return id + "," + firstName + "," + middleName + "," + lastName + "," +
				age + "," + phoneNumber + "," + profession + "," + picture;
		}
		
		public function clone():Contact {
			var contact:Contact = new Contact();
			contact.update(this);
			return contact;
		}
		
		public function compare(contact:Contact):Boolean
		{
			return ( contact.id == this.id && contact.firstName == this.firstName && contact.middleName == this.middleName && contact.lastName == this.lastName 
					 && contact.age == this.age && contact.phoneNumber == this.phoneNumber && contact.profession == this.profession && contact.picture == this.picture );
		}
		
		public function update(oldContact:Contact):void
		{
			id = oldContact.id;
			firstName = oldContact.firstName;
			middleName = oldContact.middleName;
			lastName = oldContact.lastName;
			age = oldContact.age;
			phoneNumber = oldContact.phoneNumber;
			profession = oldContact.profession;
			picture = oldContact.picture;
		}
	}
}