package com.bestsoftwareever.contactmanager.model
{
	import com.bestsoftwareever.contactmanager.services.GoogleService;
	
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;

	public class ContactsModel extends ModelBase
	{
		public static var NAME:String = "contactsModel";
		public var contacts:Array = new Array();
		public var contactsObj:Array = new Array();
		public var selectedContact:Contact = new Contact();
		
		public function ContactsModel() 
		{
			super(NAME);
		}
	}
}