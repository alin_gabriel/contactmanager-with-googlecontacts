package com.bestsoftwareever.contactmanager.model
{
	import flash.utils.Dictionary;

	public class ModelLocator
	{
		private static var INSTANCE:ModelLocator;
		private var models:Dictionary = new Dictionary();
		
		public static function getInstance():ModelLocator {
			if (!INSTANCE) {
				INSTANCE = new ModelLocator(new SingletonEnforcer());
			}
			return INSTANCE;
		}
		
		public function ModelLocator(se:SingletonEnforcer) {
			if (!se is SingletonEnforcer) {
				throw new Error("ModelLocator cannot be instantiated");
			}
		}
		
		public function getModel(name:String):ModelBase
		{
			return models[name];
		}
		
		public function addModel(name:String, model:ModelBase):void {
			models[name] = model;
		}
	}
}

class SingletonEnforcer {}