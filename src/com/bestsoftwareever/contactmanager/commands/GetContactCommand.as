package com.bestsoftwareever.contactmanager.commands
{
	import com.bestsoftwareever.contactmanager.events.GetContactEvent;
	import com.bestsoftwareever.contactmanager.model.ContactsModel;
	import com.bestsoftwareever.contactmanager.model.ModelLocator;
	import com.bestsoftwareever.contactmanager.services.GoogleService;
	
	import flash.events.Event;
	
	import mx.rpc.AsyncToken;
	import mx.rpc.Responder;

	public class GetContactCommand extends Command
	{
		public var services:GoogleService = GoogleService.getInstance();
		public var contactsModel:ContactsModel = ModelLocator.getInstance().getModel(ContactsModel.NAME) as ContactsModel;
		
		private var _token:AsyncToken;
		public function get token():AsyncToken
		{
			return _token;
		}
		
		public function set token(value:AsyncToken):void
		{
			_token = value;
			_token.addResponder(new Responder(result,fault));
		}
		
		private function fault(info:Object):void
		{
			trace(info.toString());
			trace("not ok");
		}
		
		private function result(data:Object):void
		{
			trace(data);
			trace("ok");
		}
		
		public function GetContactCommand()
		{
			super();
		}
		
		override public function execute(event:Event):void
		{	
			token = GoogleService.getInstance().getContact((event as GetContactEvent).id);
		}
		
		
	}
}