package com.bestsoftwareever.contactmanager.commands
{
	import com.bestsoftwareever.contactmanager.events.GetContactEvent;
	import com.bestsoftwareever.contactmanager.events.SetViewEvent;
	import com.bestsoftwareever.contactmanager.model.Contact;
	import com.bestsoftwareever.contactmanager.model.ContactsModel;
	import com.bestsoftwareever.contactmanager.model.ModelLocator;
	import com.bestsoftwareever.contactmanager.services.GoogleService;
	import com.bestsoftwareever.contactmanager.views.ContactsList;
	
	import flash.events.Event;
	
	import mx.rpc.AsyncToken;
	import mx.rpc.Responder;

	public class GetContactsCommand extends Command
	{
		public var services:GoogleService = GoogleService.getInstance();
		public var contactsModel:ContactsModel = ModelLocator.getInstance().getModel(ContactsModel.NAME) as ContactsModel;
		
		private var _token:AsyncToken;
		public function get token():AsyncToken
		{
			return _token;
		}
		
		public function set token(value:AsyncToken):void
		{
			_token = value;
			_token.addResponder(new Responder(result,fault));
		}
		
		private function fault(info:Object):void
		{
			trace(info.toString());
			trace("not ok");
		}
		
		private function result(data:Object):void
		{
			services.result = JSON.parse(data.result);
			contactsModel.contactsObj = services.result.feed.entry;
			for (var i:int = 0; i < contactsModel.contactsObj.length; i++) 
			{
				contactsModel.contacts[i] = new Contact();
				if ( contactsModel.contactsObj[i].hasOwnProperty("gd$email") )
					contactsModel.contacts[i].email = contactsModel.contactsObj[i].gd$email[0].address;
				if ( contactsModel.contactsObj[i].hasOwnProperty("gd$phoneNumber") )
					contactsModel.contacts[i].phoneNumber = contactsModel.contactsObj[i].gd$phoneNumber[0].$t;
				//contactsModel.
				contactsModel.contacts[i].id = contactsModel.contactsObj[i].id.$t.split("/")[contactsModel.contactsObj[i].id.$t.split("/").length - 1];
			}
			
			/*for (var i:int = 0; i < result.feed.entry.length; i++) {
				dispatcher.dispatchEvent(new GetContactEvent(result.feed.entry[i].id.$t.split("/")[result.feed.entry[i].id.$t.split("/").length - 1]));
			}*/	
			dispatcher.dispatchEvent(new SetViewEvent(ContactsList.NAME));
		}
		
		public function GetContactsCommand()
		{
			super();
		}
		
	

		override public function execute(event:Event):void
		{	
			token = GoogleService.getInstance().getContacts(1,15);
		}
		
		
	}
}