package com.bestsoftwareever.contactmanager.commands
{
	import com.bestsoftwareever.contactmanager.controller.Controller;
	import com.bestsoftwareever.contactmanager.events.AuthenticationEvent;
	import com.bestsoftwareever.contactmanager.events.LoginEvent;
	import com.bestsoftwareever.contactmanager.events.SetViewEvent;
	import com.bestsoftwareever.contactmanager.model.ModelLocator;
	import com.bestsoftwareever.contactmanager.model.ViewsModel;
	import com.bestsoftwareever.contactmanager.services.GoogleService;
	import com.bestsoftwareever.contactmanager.views.AuthenticationView;
	import com.bestsoftwareever.contactmanager.views.ContactsList;
	
	import events.GetAccessTokenEvent;
	
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.geom.Rectangle;
	import flash.media.StageWebView;
	import flash.net.Responder;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import grant.AuthorizationCodeGrant;
	import grant.IGrantType;
	
	import mx.rpc.AsyncToken;

	public class LoginCommand extends Command
	{	
		
		public function LoginCommand()
		{
			super();
		}
		
		override public function execute(event:Event):void
		{	
			//dispatcher.dispatchEvent(new SetViewEvent(AuthenticationView.NAME));
			dispatcher.dispatchEvent(new AuthenticationEvent(AuthenticationEvent.NAME));
		}
		
	}
}