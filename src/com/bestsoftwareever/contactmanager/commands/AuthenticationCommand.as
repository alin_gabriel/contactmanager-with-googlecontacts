package com.bestsoftwareever.contactmanager.commands
{
	import com.bestsoftwareever.contactmanager.events.GetContactsEvent;
	import com.bestsoftwareever.contactmanager.events.SetViewEvent;
	import com.bestsoftwareever.contactmanager.model.ModelLocator;
	import com.bestsoftwareever.contactmanager.model.ViewsModel;
	import com.bestsoftwareever.contactmanager.services.GoogleService;
	import com.bestsoftwareever.contactmanager.views.AuthenticationView;
	import com.bestsoftwareever.contactmanager.views.ContactsList;
	
	import events.GetAccessTokenEvent;
	
	import flash.events.Event;
	import flash.media.StageWebView;
	
	import grant.AuthorizationCodeGrant;
	import grant.IGrantType;

	public class AuthenticationCommand extends Command
	{
		private var stageWebView:StageWebView;
		private var service:GoogleService = GoogleService.getInstance();

		private var grant:IGrantType;
		
		public function AuthenticationCommand()
		{
		}
		
		override public function execute(event:Event):void
		{
			stageWebView = ((ModelLocator.getInstance().getModel(ViewsModel.NAME) as ViewsModel).getView(AuthenticationView.NAME) as AuthenticationView).stageWebView;
			
			grant = new AuthorizationCodeGrant(stageWebView,                     // the StageWebView object for which to display the user consent screen
				"610147619577-h8d6nmj7vvop82kk8thatqj4jgres8tg.apps.googleusercontent.com",          // your client ID
				"-tttMkzqqlQACunDfElqJiP8",      // your client secret
				"urn:ietf:wg:oauth:2.0:oob",       // your redirect URI
				"https://www.google.com/m8/feeds/"              // (optional) your scope
			);             // (optional) your state
			
			service.oauth2.addEventListener(GetAccessTokenEvent.TYPE, onGetAccessToken);
			service.oauth2.getAccessToken(grant);
		}
		
		protected function onGetAccessToken(getAccessTokenEvent:GetAccessTokenEvent):void
		{
			if (getAccessTokenEvent.errorCode == null && getAccessTokenEvent.errorMessage == null)
			{
				stageWebView.dispose();
				service.accessToken = getAccessTokenEvent.accessToken;
				service.login("qwe","asd");
				dispatcher.dispatchEvent(new GetContactsEvent());
			}
		}  // onGetAccessToken
	}
}